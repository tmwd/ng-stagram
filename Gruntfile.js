module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        concat: {
            js: {
                src: ['./public/src/js/*'],
                dest: './public/dist/js/<%= pkg.name %>-<%= pkg.version %>.js'
            },
            css: {
                src: ['./public/src/css/*.css'],
                dest: './public/dist/css/<%= pkg.name %>-<%= pkg.version %>.css'
            }
        },

        csscomb: {
            css: {
                options: {
                    config: './csscomb_config.json'
                },
                files: {
                    './public/dist/css/<%= pkg.name %>-<%= pkg.version %>.css': ['./public/dist/css/<%= pkg.name %>-<%= pkg.version %>.css']
                }
            }
        },

        cssmin: {
            combine: {
                files: {
                    './public/dist/css/<%= pkg.name %>-<%= pkg.version %>.min.css': ['./public/dist/css/<%= pkg.name %>-<%= pkg.version %>.css']
                }
            }
        },

        uglify: {
            my_target: {
                files: {
                    './public/dist/js/<%= pkg.name %>-<%= pkg.version %>.min.js': ['./public/dist/js/<%= pkg.name %>-<%= pkg.version %>.js']
                }
            }
        },

        less: {
            production: {
                files: {
                    './public/src/css/all.css': './public/src/less/all.less'
                }
            }
        },

        watch: {
            css: {
                files: ['./public/src/less/*.less'],
                tasks: ['less']
            }
        }

    });

    grunt.registerTask('build', ['less', 'concat', 'csscomb', 'cssmin', 'uglify']);
};