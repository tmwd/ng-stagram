(function () {

    var socket = io();

    socket.on('info', function (data) {
        console.log(data);
    });


    var app = angular.module('ngStagram', ['ngCookies']);

    app.factory('user', ['$rootScope', '$http', function ($rootScope, $http) {

        var user = {};

        user.self = {
            token: null,
            id: null,
            name: null,
            pic: null,
            follows: null,
            followed_by: null,
            getSelf: function () {
                var self = this;
                $http.jsonp('https://api.instagram.com/v1/users/self?access_token=' + self.token + '&callback=JSON_CALLBACK').success(function (data) {
                    try {
                        self.id = data.data.id;
                        self.name = data.data.username;
                        self.pic = data.data.profile_picture;
                        self.follows = data.data.counts.follows;
                        self.followed_by = data.data.counts.followed_by;
                        $rootScope.$broadcast('loggedIn');
                    } catch (e) {
//                        console.log(e);
                    }
                });
            }
        };

        user.feed = {
            data: [],
            next: null,
            load: function () {
                var self = this;
                $http.jsonp((self.next || 'https://api.instagram.com/v1/users/self/feed?access_token=' + user.self.token) + '&callback=JSON_CALLBACK').success(function (data) {
                    try {
                        self.data = self.data.concat(data.data);
                        self.next = data.pagination.next_url;
                        $rootScope.$broadcast('feedUpdated');
                    } catch (e) {
//                        console.log(e);
                    }
                });
            },
            reset: function () {
                this.data = [];
                this.next = null;
                $rootScope.$broadcast('feedUpdated');
            }
        };

        user.followers = {
            data: [],
            next: null,
            load: function () {
                var self = this;
                $http.jsonp((self.next || 'https://api.instagram.com/v1/users/' + user.self.id + '/followed-by?access_token=' + user.self.token) + '&callback=JSON_CALLBACK').success(function (data) {
                    try {
                        self.data = self.data.concat(data.data);
                        self.next = data.pagination.next_url;
                        $rootScope.$broadcast('followersUpdated');
                    } catch (e) {
//                        console.log(e);
                    }
                });
            },
            reset: function () {
                this.data = [];
                this.next = null;
                $rootScope.$broadcast('followersUpdated');
            }
        };

        user.resetContent = function () {
            this.feed.reset();
            this.followers.reset();
        };

        user.like = function (id) {
            socket.emit('like', id);
        };

        return user;
    }]);


    app.controller('ProfileCtrl', ['$scope', '$rootScope', 'user', function ($scope, $rootScope, user) {

        var self = this;
        self.is_authorized = false;
        self.content = null;

        $scope.$on('loggedIn', function () {
            self.is_authorized = true;
            self.name = user.self.name;
            self.pic = user.self.pic;
            self.follows = user.self.follows;
            self.followed_by = user.self.followed_by;
            self.loadFeed();
        });

        self.loadFeed = function () {
            if (self.content == 'feed') return;
            user.resetContent();
            $rootScope.$broadcast('loadFeed');
            self.content = 'feed';
        };

        self.loadFollowers = function () {
            if (self.content == 'followers') return;
            user.resetContent();
            $rootScope.$broadcast('loadFollowers');
            self.content = 'followers';
        };

    }]);

    app.controller('AuthCtrl', ['$cookies', '$location', 'user', function ($cookies, $location, user) {

        var cookie_token = $cookies.token,
            url_token = $location.path().split('=')[1] || null;

        if (cookie_token || url_token) {

            $cookies.token = user.self.token = cookie_token || url_token;
            user.self.getSelf();
            socket.emit('access_token', user.self.token);
            if (url_token) $location.path('/');

        } else {

            this.client_id = '1cd661bda1974faa8c9a4cf808697491';
            this.redirect_uri = encodeURIComponent('http://localhost:9000');
            this.href = 'https://instagram.com/oauth/authorize/?client_id=' + this.client_id + '&redirect_uri=' + this.redirect_uri + '&response_type=token&scope=basic+likes+comments+relationships';
        }

    }]);

    app.controller('FeedCtrl', ['$scope', 'user', function ($scope, user) {

        this.loadMore = function () {
            user.feed.load();
        };

        this.like = function (id) {
            user.like(id);
        };

        $scope.$on('loadFeed', function () {
            user.feed.load();
        });

        $scope.$on('feedUpdated', function () {
            $scope.feed = user.feed.data;
        });

    }]);

    app.controller('FollowersCtrl', ['$scope', 'user', function ($scope, user) {

        this.loadMore = function () {
            user.followers.load();
        };

        $scope.$on('loadFollowers', function () {
            user.followers.load();
        });

        $scope.$on('followersUpdated', function () {
            $scope.followers = user.followers.data;
        });

    }]);

}());