var express = require('express'),
    app = express(),
    server = require('http').createServer(app),
    io = require('socket.io')(server);

app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
    res.sendfile('index.html');
});

io.on('connection', function (socket) {

    var ig = require('instagram-node').instagram();
    ig.use({
        client_id: '1cd661bda1974faa8c9a4cf808697491',
        client_secret: 'af743eb30d794c9d9341b5e40ce63615'
    });

    socket.on('access_token', function (data) {
        ig.use({access_token: data});
    });

    socket.on('like', function (data) {
        ig.add_like(data, function (err, remaining, limit) {
            socket.emit('info', 'like');
        });
    });

});

server.listen(3000);